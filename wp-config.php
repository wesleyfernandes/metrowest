<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'bd_metrowest' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', 'root' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '>pFE(:2_;K:-YV&$Q,b(;i,YC+Qg@U1,L6aj,pPv`%CfViPpM&Kr*MN`KW;gUUNr' );
define( 'SECURE_AUTH_KEY',  '@H37[~&<6wC=<]mE%;E|a+y%dF?(r=]J;um,i39U^hm4YQr~xPuV,YF[hL!O*j$(' );
define( 'LOGGED_IN_KEY',    'S:+Nkcc`<{Dp2%llwMCl1fWr> %D5.u&@r9c-NV5|o*h#1w.uM]Z?VUHA{@5_:>^' );
define( 'NONCE_KEY',        'ejCG$7:~%O09xZ4mm2G9VODZaoIVH!;on/7g5pbyrTy=9iS;cLSjX.^,xPx)+Q1c' );
define( 'AUTH_SALT',        '_C}9F]e#RC1yq[iHU0QRaw4DXOvtmV6amUK3-mW*T(QlMQ~2uS>M~o2|@`Y9{ #u' );
define( 'SECURE_AUTH_SALT', 'I&gC!KeT37IgN9@9XUO.dg9<?xA)_if6aD7;QiTH?iB:8HtK$Mj:!:#$>yM 5W2/' );
define( 'LOGGED_IN_SALT',   '|S)gKV(U9ETK%:L#c~ax+w`>hc{*ZJs+O]BZ& fwIpH>w3fw)ji^,@z>O1})V^?4' );
define( 'NONCE_SALT',       '!%y>09Nm~f*MitK#J@:a!Y(2(=D3bKgx.%!gof1]rQ@xaKcFG1w,Z[5Rj6qYg)dE' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'mt_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
