<div id="surstudio_plugin_translator_revolution_lite_settings">

<form method="post" enctype="multipart/form-data" action="" name="surstudio_admin" id="surstudio_admin">

<div class="surstudio_plugin_translator_revolution_lite_page_title_container">
	<div class="surstudio_plugin_translator_revolution_lite_da_icon_main">
		<div class="dashicons-before dashicons-translation"></div>
	</div>
	<h2 class="surstudio_plugin_translator_revolution_lite_page_title">Translator Revolution</h2>
</div>

<div class="surstudio_plugin_translator_revolution_lite_admin_container">

	<div class="surstudio_plugin_translator_revolution_lite_ui_tabs_container surstudio_plugin_translator_revolution_lite_no_display" id="surstudio_plugin_translator_revolution_lite_main_navigation">
		<ul>
			<li class="surstudio_plugin_translator_revolution_lite_ui_tab surstudio_plugin_translator_revolution_lite_ui_tab_{{ translations.show.false:begin }}un{{ translations.show.false:end }}selected" id="translations_menu">
				<div class="dashicons-before dashicons-index-card"></div>
				<span><span>{{ translations_message }}</span></span>
			</li> 
		</ul>
	</div>

	<div class="surstudio_plugin_translator_revolution_lite_main_form_container">
	
		<div class="surstudio_plugin_translator_revolution_lite_ui_tabs_main_container">

			<div class="surstudio_plugin_translator_revolution_lite_ui_tab_container surstudio_plugin_translator_revolution_lite_{{ translations.show.false:begin }}no_{{ translations.show.false:end }}display" id="translations_tab">
				<div class="surstudio_plugin_translator_revolution_lite_ui_tab_content">

					<div class="surstudio_plugin_translator_revolution_lite_ui_tabs_container surstudio_plugin_translator_revolution_lite_ui_tabs_container_alt">
						<ul>
						   <li class="surstudio_plugin_translator_revolution_lite_ui_tab surstudio_plugin_translator_revolution_lite_ui_tab_{{ translations_general.show.false:begin }}un{{ translations_general.show.false:end }}selected" id="translations_general_menu"><div class="dashicons-before dashicons-list-view"></div><span><span>{{ translations_general_message }}</span></span></li> 
						   <li class="surstudio_plugin_translator_revolution_lite_ui_tab surstudio_plugin_translator_revolution_lite_ui_tab_{{ translations_import.show.false:begin }}un{{ translations_import.show.false:end }}selected" id="translations_import_menu"><div class="dashicons-before dashicons-download"></div><span><span>{{ translations_import_message }}</span></span></li> 
						   <li class="surstudio_plugin_translator_revolution_lite_ui_tab surstudio_plugin_translator_revolution_lite_ui_tab_{{ translations_export.show.false:begin }}un{{ translations_export.show.false:end }}selected" id="translations_export_menu"><div class="dashicons-before dashicons-upload"></div><span><span>{{ translations_export_message }}</span></span></li> 
						</ul>
					</div>

					<div class="surstudio_plugin_translator_revolution_lite_main_form_container">
			
						<div class="surstudio_plugin_translator_revolution_lite_ui_tabs_main_container">

							<div class="surstudio_plugin_translator_revolution_lite_ui_tab_container surstudio_plugin_translator_revolution_lite_{{ translations_general.show.false:begin }}no_{{ translations_general.show.false:end }}display" id="translations_general_tab">

								<div class="surstudio_plugin_translator_revolution_lite_ui_tab_content">
									
									{{ group_4 }}

								</div>

							</div>

							<div class="surstudio_plugin_translator_revolution_lite_ui_tab_container surstudio_plugin_translator_revolution_lite_{{ translations_import.show.false:begin }}no_{{ translations_import.show.false:end }}display" id="translations_import_tab">

								<div class="surstudio_plugin_translator_revolution_lite_ui_tab_content">
									
									{{ group_6 }}
		
								</div>
									
							</div>
						
							<div class="surstudio_plugin_translator_revolution_lite_ui_tab_container surstudio_plugin_translator_revolution_lite_{{ translations_export.show.false:begin }}no_{{ translations_export.show.false:end }}display" id="translations_export_tab">

								<div class="surstudio_plugin_translator_revolution_lite_ui_tab_content">

									{{ group_7 }}

								</div>
									
							</div>

							<div class="surstudio_plugin_translator_revolution_lite_ui_tab_container surstudio_plugin_translator_revolution_lite_{{ translations_permissions.show.false:begin }}no_{{ translations_permissions.show.false:end }}display" id="translations_permissions_tab">

								<div class="surstudio_plugin_translator_revolution_lite_ui_tab_content">

									{{ group_9 }}

								</div>
									
							</div>

						</div>
						
					</div>

				</div>
			</div>
			
		</div>
	</div>

	<input type="hidden" name="surstudio_plugin_translator_revolution_lite_admin_action" id="surstudio_plugin_translator_revolution_lite_admin_action" value="surstudio_plugin_translator_revolution_lite_save_settings" />
	<input type="hidden" name="surstudio_tab" id="surstudio_tab" value="{{ tab }}" />
	<input type="hidden" name="surstudio_tab_2" id="surstudio_tab_2" value="{{ tab_2 }}" />
	<input type="hidden" name="surstudio_tab_3" id="surstudio_tab_3" value="{{ tab_3 }}" />

</form>

</div>

<style type="text/css">
.surstudio_plugin_translator_revolution_lite_admin_container {
	border-top: 1px solid #bbb;
}
</style>
<script type="text/javascript">
/*<![CDATA[*/
SurStudioPluginTranslatorRevolutionLiteAdmin.initialize("{{ ajax_url }}");
jQuery("#translations_menu").click();
/*]]>*/
</script>